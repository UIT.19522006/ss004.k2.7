#include <iostream>
#include <windows.h>
using namespace std;

#define BLACK_COLOR			0
#define DARK_BLUE_COLOR		1
#define DARK_GREEN_COLOR	2
#define DARK_CYAN_COLOR		3
#define DARK_RED_COLOR		4
#define DARK_PINK_COLOR		5
#define DARK_YELLOW_COLOR	6
#define DARK_WHITE_COLOR	7
#define GREY_COLOR			8
#define BLUE_COLOR			9
#define GREEN_COLOR			10
#define CYAN_COLOR			11
#define RED_COLOR			12
#define PINK_COLOR			13
#define YELLOW_COLOR		14
#define WHITE_COLOR			15

void TextColor (int color)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE) , color);
}

void Menu()
{
    TextColor(10);
    cout <<"\n\n\t\t\t\t\t\t ========== MENU ==========";
    cout << "\n\t\t\t\t\t\t\t 1. Play" ;
    cout << "\n\t\t\t\t\t\t\t 2. Hight score";
    cout << "\n\t\t\t\t\t\t\t 3. Quick tutor";
    cout << "\n\t\t\t\t\t\t\t 4. Exit";
    cout <<"\n\t\t\t\t\t\t =========== END ===========";
}


void Quick_tutor()
{
    TextColor(11);
    cout <<"\n\n\t\t\t\t\t\t ======== QUICK TUTOR ========";
    cout << "\n\t\t\t\t\t\t\t W : up" ;
    cout << "\n\t\t\t\t\t\t\t D : down";
    cout << "\n\t\t\t\t\t\t\t A : left";
    cout << "\n\t\t\t\t\t\t\t S : right";
    cout << "\n\t\t\t\t\t\t\t S : to pause";
    cout << "\n\t\t\t\t\t\t\t Esc : to exit";
    cout <<"\n\t\t\t\t\t\t ============ END ============";
}

void To_exit()
{
    TextColor(12);
    cout <<"\n\n\t\t\t\t\t\t ========== QUERY ==========";
    cout << "\n\t\t\t\t\t\t   Are you sure to quit ??? ";
    cout << "\n\t\t\t\t\t\t\t 1. Yes";
    cout << "\n\t\t\t\t\t\t\t 2. No";
    cout <<"\n\t\t\t\t\t\t =========== END ===========";
}

int main()
{
    Menu();
    Quick_tutor();
    To_exit();
    return 0;
}
