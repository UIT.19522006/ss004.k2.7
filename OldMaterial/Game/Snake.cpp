#include "Snake.h"
void SetFontSize(const int& x, const int& y)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_FONT_INFOEX lpConsoleCurrentFontEx;
	lpConsoleCurrentFontEx.cbSize = sizeof(lpConsoleCurrentFontEx);
	lpConsoleCurrentFontEx.dwFontSize.X = x;
	lpConsoleCurrentFontEx.dwFontSize.Y = y;
	lpConsoleCurrentFontEx.FontFamily = FF_DONTCARE;
	lpConsoleCurrentFontEx.FontWeight = FW_BOLD;
	SetCurrentConsoleFontEx(out, false, &lpConsoleCurrentFontEx);
}
void SetWindowSize(const int& x, const int& y)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, x, y, TRUE);
}
void CenterWindow()
{
	HWND console = GetConsoleWindow();
	RECT rectClient, rectWindow;
	GetClientRect(console, &rectClient);
	GetWindowRect(console, &rectWindow);
	int px, py;
	px = GetSystemMetrics(SM_CXSCREEN) / 2 - (rectWindow.right - rectWindow.left) / 2;
	py = GetSystemMetrics(SM_CYSCREEN) / 2 - (rectWindow.bottom - rectWindow.top) / 2;
	MoveWindow(console, px, py, rectClient.right - rectClient.left, rectClient.bottom - rectClient.top, TRUE);
}
void FixWindowSize()
{
	HWND consoleWindow = GetConsoleWindow();
	LONG style = GetWindowLong(consoleWindow, GWL_STYLE);
	style = style & ~(WS_MAXIMIZEBOX) & ~(WS_THICKFRAME);
	SetWindowLong(consoleWindow, GWL_STYLE, style);
}
void Nocursortype() // An con tro tren man hinh console;
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO Info;
	Info.bVisible = FALSE;
	Info.dwSize = 20;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
}
void StandardScreen()
{
	SetFontSize(18, 18);
	SetWindowSize(1920, 1080);
	CenterWindow();
	FixWindowSize();
	Nocursortype();
}
void OutputMenu(const int& k)
{
	TextColor(15);
	cout << "\n\n\t\t ========== MENU ==========";
	if (k == 1)
	{
		TextColor(10);
		cout << "\n\t\t\t >  Play";
		TextColor(15);
	}
	else cout << "\n\t\t\t    Play";
	if (k == 2)
	{
		TextColor(10);
		cout << "\n\t\t\t >  High score";
		TextColor(15);
	}
	else cout << "\n\t\t\t    High score";
	if (k == 3)
	{
		TextColor(10);
		cout << "\n\t\t\t >  Quick tutor";
		TextColor(15);
	}
	else cout << "\n\t\t\t    Quick tutor";
	if (k == 4)
	{
		TextColor(10);
		cout << "\n\t\t\t >  Exit";
		TextColor(15);
	}
	else cout << "\n\t\t\t    Exit";
	cout << "\n\t\t =========== END ===========";
}
int ChoseMap()
{
	system("cls");
	int k = 1;
	while (1)
	{
		MoveCursor(0, 0);
		if (_kbhit())
		{
			char a = _getch();
			if (a == 13)
			{
				return (k - 1);
			}
			if (a == 72)
			{
				k--;
				if (k == 0) k = 2;
			}
			if (a == 80)
			{
				k++;
				if (k == 3) k = 1;
			}
		}
		cout << "\t\t\t===Chon map===\n";
		if (k == 1)
		{
			TextColor(13);
			cout << "\t\t\t > Map 1";
			TextColor(15);
		}
		else cout << "\t\t\t   Map 1";
		cout << "\n";
		if (k == 2)
		{
			TextColor(13);
			cout << "\t\t\t > Map 2";
			TextColor(15);
		}
		else cout << "\t\t\t   Map 2";
	}
}
void RunMenu()
{
	OutputMenu(1);
	int arrow = 1;
	while (1)
	{
		if (_kbhit())
		{
			char c = _getch();
			if (c == 13)
			{
				if (arrow == 1)
				{
					//// Start Game
					Snake snake(ChoseMap());
					snake.InitDisplay();
					snake.Play();
					return;
				}
				if (arrow == 2)
				{
					HighScore();
					system("cls");
					OutputMenu(arrow);
				}
				if (arrow == 3)
				{
					Tutor();
					system("cls");
					OutputMenu(arrow);
				}
				if (arrow == 4)
				{
					Exit();
					system("cls");
					OutputMenu(arrow);
				}
			}
			if (c == 32)
			{
				if (arrow == 4) arrow = 1;
				else arrow++;
				MoveCursor(0, 0);
				OutputMenu(arrow);
			}
			int a = (int)c;
			if (a == 80)
			{
				if (arrow == 4) arrow = 1;
				else arrow++;
				OutputMenu(arrow);
			}
			if (a == 72)
			{
				if (arrow == 1) arrow = 4;
				else arrow--;
				OutputMenu(arrow);
			}
		}
		MoveCursor(0, 0);
	}
}
void Tutor()
{
	system("cls");
	TextColor(14);
	cout << "\n\n\t\t\t ======== QUICK TUTOR ========";
	cout << "\n\t\t\t\t W or Arrow up: up";
	cout << "\n\t\t\t\t D or Arrow down: down";
	cout << "\n\t\t\t\t A or Arrow left: left";
	cout << "\n\t\t\t\t S or Arrow right: right";
	cout << "\n\t\t\t\t Space Bar : pause and move selection";
	cout << "\n\t\t\t\t Esc : exit";
	cout << "\n\t\t\t\t Enter : chose";
	cout << "\n\t\t\t ============ END ============";
	TextColor(15);
	_getch();
}
void Exit()
{
	system("cls");
	TextColor(6);
	int k = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 80 || a == 72 || a == 32)
			{
				if (k == 1) k = 0;
				else k = 1;
			}
			if (a == 13)
			{
				if (k == 0)
				{
					TextColor(15);
					system("cls");
					system("PAUSE");
					exit(0);
				}
				else return;
			}
		}
		cout << "\n\n\t\t\t ========== QUERY ==========";
		cout << "\n\t\t\t   Are you sure to quit ??? ";
		if (k == 0)
		{
			TextColor(14);
			cout << "\n\t\t\t\t > Yes";
			TextColor(6);
		}
		else cout << "\n\t\t\t\t   Yes";
		if (k == 1)
		{
			TextColor(14);
			cout << "\n\t\t\t\t > No";
			TextColor(6);
		}
		else cout << "\n\t\t\t\t   No";
		cout << "\n\t\t\t =========== END ===========";
		MoveCursor(0, 0);
	}
	TextColor(15);

}
void HighScore()
{
	system("cls");
	ifstream f;
	f.open("HighScore.txt", ios::in);
	if (!f.is_open())
	{
		cout << "Can't open HighScore.txt";
		_getch();
		return;
	}
	vector<string> name;
	vector<int> score;
	while (!f.eof())
	{
		string a;
		string b;
		int diem;

		getline(f, a);
		if (a.size() == 0) break;

		int pos = a.find(':');
		for (int i = pos + 1; i < a.size(); i++)
		{
			b += a[i];
		}
		a.erase(a.begin() + pos, a.end());
		stringstream ss(b);
		ss >> diem;

		name.push_back(a);
		score.push_back(diem);
	}
	cout << "\n\n\n\n\n";
	cout << "                      =======High scores=======\n";
	if (name.size() == 0)
	{
		cout << "\n";
		cout << "                               No data";
		cout << "\n\n";
	}
	else
		for (int i = 0; i < name.size(); i++)
		{
			cout << i + 1 << ". " << name[i] << " : " << score[i] << "\n\n";
		}
	cout << "\n";
	cout << "                       ==========END=========";
	f.close();
	_getch();
}
void OutputIntro()
{
	system("cls");
	TextColor(12);
	cout << "\n\n\n\n\n";
	cout << "    ||========  ||\\\\     ||        //\\\\        ||    //  ||=========" << endl;
	cout << "    ||          || \\\\    ||       //  \\\\       ||  //    ||" << endl;
	cout << "    ||          ||  \\\\   ||      //    \\\\      ||//      ||" << endl;
	cout << "    ||========  ||   \\\\  ||     //======\\\\     ||        ||=========" << endl;
	cout << "            ||  ||    \\\\ ||    //        \\\\    ||\\\\      ||" << endl;
	cout << "            ||  ||     \\\\||   //          \\\\   ||  \\\\    ||" << endl;
	cout << "    ========||  ||      \\\\|  //            \\\\  ||    \\\\  ||=========" << endl << "\n";
	TextColor(1);
	cout << "                                                   SS004.K2.7" << "\n";
	TextColor(2);
	cout << "                                            19520022: Pham Ngoc Cam     " << "\n";
	cout << "                                            19520072: Le Kim Hang       " << "\n";
	cout << "                                            19520365: Do Thi Thanh An   " << "\n";
	cout << "                                            19521922: Tran Luong Nguyen " << "\n";
	cout << "                                            19522006: Nguyen Cong Phi   " << "\n";
	TextColor(15);
	int c = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 13)
			{
				break;
			}
		}
		Sleep(400);
		MoveCursor(19, 19);
		if (c == 0)
		{
			cout << "     Press Enter to continue     ";
			c++;
		}
		else
		{
			cout << "                                   ";
			c--;
		}
	}
	system("cls");
	MoveCursor(0, 0);
}
void TextColor(int color)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}
void MoveCursor(const int& x, const int& y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsole, c);
}
Snake::Snake()
{
}
Snake::Snake(int m)
{
	if (m == 1)
	{
		mode = 1;
		GetMap(1);
		Spawn();
	}
	else
	{
		mode = 0;
		GetMap(2);
		Spawn();
	}
}
void Snake::Spawn()
{
	Point p;
	p.x = 3;
	p.y = 3;
	Ran.push_back(p);
	p.y--;
	Ran.push_back(p);
	p.y--;
	Ran.push_back(p);
}
void Snake::Move(int way)
{
	if (way == 1)
	{
		MoveCursor( Ran[Ran.size() - 1].y + posy, Ran[Ran.size() - 1].x +posx );
		cout << " ";
		Point a = Ran[0];
		a.x--;
		if (a.x < 0)
		{
			a.x = 29;
		}
		MoveCursor(a.y + posy, a.x + 1 + posx);
		cout << "X";
		MoveCursor(a.y + posy, a.x + posx);
		cout << "R";
		Ran.insert(Ran.begin(), a);
		Ran.erase(Ran.begin() + Ran.size() - 1);
	}
	if (way == 2)
	{
		MoveCursor(Ran[Ran.size() - 1].y + posy, Ran[Ran.size() - 1].x + posx);
		cout << " ";
		Point a = Ran[0];
		a.y--;
		if (a.y < 0)
		{
			a.y = 29;
		}
		MoveCursor(a.y+1 + posy, a.x + posx);
		cout << "X";
		MoveCursor(a.y + posy, a.x + posx);
		cout << "R";
		Ran.insert(Ran.begin(), a);
		Ran.erase(Ran.begin() + Ran.size() - 1);
	}
	if (way == 3)
	{
		MoveCursor(Ran[Ran.size() - 1].y + posy, Ran[Ran.size() - 1].x + posx);
		cout << " ";
		Point a = Ran[0];
		a.x++;
		if (a.x > 29)
		{
			a.x = 0;
		}
		MoveCursor(a.y + posy, a.x - 1 + posx);
		cout << "X";
		MoveCursor(a.y + posy, a.x + posx);
		cout << "R";
		Ran.insert(Ran.begin(), a);
		Ran.erase(Ran.begin() + Ran.size() - 1);
	}
	if (way == 4)
	{
		MoveCursor(Ran[Ran.size() - 1].y + posy, Ran[Ran.size() - 1].x + posx);
		cout << " ";
		Point a = Ran[0];
		a.y++;
		if (a.y > 29)
		{
			a.y = 0;
		}
		MoveCursor(a.y-1 + posy, a.x + posx);
		cout << "X";
		MoveCursor(a.y + posy, a.x + posx);
		cout << "R";
		Ran.insert(Ran.begin(), a);
		Ran.erase(Ran.begin() + Ran.size() - 1);
	}

}
void Snake::InitDisplay()
{
	vector<string> title;
	string a = "------------------------------";
	string b = "---          SNAKE         ---";
	title.push_back(a);
	title.push_back(b);
	title.push_back(a);

	string c, d;
	vector<string> Gamestate;
	a = "----------------------------";
	b = "          GAME STATE        ";
	c = "    --------------------    ";
	d = "          \u001b[36;1mPLAYING\u001b[0m          ";

	Gamestate.push_back(a);
	Gamestate.push_back(b);
	Gamestate.push_back(c);
	Gamestate.push_back(d);
	Gamestate.push_back(a);

	vector<string> Map;
	Map.push_back(a);
	b = "             MAP            ";
	Map.push_back(b);
	Map.push_back(a);

	vector<string> Score;
	Score.push_back(a);
	b = "             SCORE          ";
	Score.push_back(b);
	Score.push_back(c);
	d = "           00000000         ";
	Score.push_back(d);

	string lim(73, ' ');

	for (int i = 0; i < 36; i++)
	{
		Display.push_back(lim);
	}


	MoveCursor(0, 0);
	for (int i = 0; i < title.size(); i++)
	{
		for (int j = 0; j < title[i].size(); j++)
		{
			Display[i + 1][j + 2] = title[i][j];
		}
	}
	for (int i = 0; i < Gamestate.size(); i++)
	{
		for (int j = 0; j < Gamestate[i].size(); j++)
		{
			Display[i + 5][j + 35] = Gamestate[i][j];
		}
	}
	for (int i = 0; i < Map.size(); i++)
	{
		for (int j = 0; j < Map[i].size(); j++)
		{
			Display[i + 15][j + 35] = Map[i][j];
		}
	}
	for (int i = 0; i < Score.size(); i++)
	{
		for (int j = 0; j < Score[i].size(); j++)
		{
			Display[i + 25][j + 35] = Score[i][j];
		}
	}
}
void Snake::GameOver()
{
	MoveCursor(Ran[0].y + posy, Ran[0].x + posx);
	TextColor(12);
	cout << "R";
	TextColor(15);
	Sleep(2000);
	_getch();
	system("cls");
	cout << "\n\n\n";
	cout << "\t\t\t===== Game Over=====";
	cout << "\n\n\n\n";
	int k = 1;
	_getch();
	system("cls");

	//Save game
	ifstream f;
	f.open("HighScore.txt", ios::in);
	if (!f.is_open())
	{
		cout << "Can't open HighScore.txt";
		_getch();
		return;
	}
	vector<string> name;
	vector<int> scores;
	while (!f.eof())
	{
		string a;
		string b;
		int diem;

		getline(f, a);
		if (a.size() == 0) break;

		int pos = a.find(':');
		for (int i = pos + 1; i < a.size(); i++)
		{
			b += a[i];
		}
		a.erase(a.begin() + pos, a.end());
		stringstream ss(b);
		ss >> diem;

		name.push_back(a);
		scores.push_back(diem);

	}
	f.close();

	if (scores.size() == 0 || scores.size() < 10 || score > scores[scores.size() - 1])
	{
		system("cls");
		cout << "Enter your name to save: ";
		string username;
		getline(cin, username);
		if (scores.size() == 0)
		{
			scores.push_back(score);
			name.push_back(username);
		}
		else
		{
			int i = 0;
			while (i < scores.size() && score < scores[i])
			{
				i++;
			}
			if (i >= scores.size())
			{
				scores.push_back(score);
				name.push_back(username);
			}
			else
			{
				scores.insert(scores.begin() + i, score);
				name.insert(name.begin() + i, username);
			}
		}
		cout << "a";
		if (scores.size() > 10)
		{
			scores.pop_back();
			name.pop_back();
		}
	}

	ofstream f1;
	f1.open("HighScore.txt", ios::trunc);
	if (!f1.is_open())
	{
		cout << "Can't open file to write";
		RunMenu();
	}
	for (int i = 0; i < scores.size(); i++)
	{
		f1 << name[i] << ':';
		f1 << scores[i];
		f1 << "\n";
	}
	f1.close();

	system("cls");
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 13)
			{
				if (k == 1)
				{
					system("cls");
					RunMenu();
				}
				else
				{
					return;
				}
			}
			if (a == 72 || a == 80)
			{
				if (k == 1) k = 2;
				else k = 1;
			}
		}

		MoveCursor(0, 0);
		cout << "\t\t===Do you want to play again ???===\n";
		if (k == 1)
		{
			TextColor(13);
			cout << "\t\t\t > Yes";
			TextColor(15);
		}
		else cout << "\t\t\t   Yes";
		cout << "\n";
		if (k == 2)
		{
			TextColor(13);
			cout << "\t\t\t > No";
			TextColor(15);
		}
		else cout << "\t\t\t   No";
	}
}
void Snake::OutputDisplay()
{
	string display;
	for (int i = 0; i < Display.size(); i++)
	{
		display += Display[i] + "\n";
	}
	cout << display;
}
void Snake::Draw()
{
	string draw;
	for (int i = 0; i < game.size(); i++)
	{
		for (int j = 0; j < game[i].length(); j++)
		{
			Display[i + 5][j + 2] = game[i][j];
		}
	}
	for (int i = 0; i < Display.size(); i++)
	{
		draw += Display[i] + "\n";
	}
	cout << draw;
}
void Snake::EatFruit(int& count)
{
	for (int i = 0; i < Fruit.size(); i++)
	{
		if (Ran[0].x == Fruit[i].x && Ran[0].y == Fruit[i].y)
		{
			count--;
			Point a = Ran[Ran.size() - 1];
			Point b = Ran[Ran.size() - 2];
			Point Tail;
			if (a.x == b.x)
			{
				if (a.y == 0)
				{
					Tail.x = a.x;
					Tail.y = 1;
				}
				else if (a.y == 29)
				{
					Tail.x = a.x;
					Tail.y = 28;
				}
				else if (a.y > b.y)
				{

					Tail.x = a.x;
					Tail.y = a.y + 1;
				}
				else
				{
					Tail.x = a.x;
					Tail.y = a.y - 1;
				}

				Ran.push_back(Tail);

			}
			else
			{
				if (a.x == 0)
				{
					Tail.x = 1;
					Tail.y = a.y;
				}
				else if (a.x == 29)
				{
					Tail.x = 28;
					Tail.y = a.y;
				}
				else if (a.x > b.x)
				{
					Tail.x = a.x + 1;
					Tail.y = a.y;
				}
				else
				{
					Tail.x = a.x - 1;
					Tail.y = a.y;
				}
				Ran.push_back(Tail);
			}
			Fruit.erase(Fruit.begin() + i);
			//Diem
			score += speed;
			string h = to_string(score);
			while (h.size() < 8)
			{
				h = '0' + h;
			}
			TextColor(11);
			MoveCursor(46, 28);
			cout << h;
			TextColor(15);
		}
	}
	SpawnFruit(count);
}
void Snake::SpawnFruit(int& count)
{
	while (count < maxFruit)
	{
		int iReSpawn = 0;
		Point F;
		F.x = rand() % 25 + 2;
		F.y = rand() % 25 + 2;
		for (int i = 0; i < PointMap.size(); i++)
		{
			if (PointMap[i].x == F.x && PointMap[i].y == F.y)
			{
				iReSpawn = 1;
				break;
			}

		}
		for (int i = 0; i < Ran.size(); i++)
		{
			if (Ran[i].x == F.x && Ran[i].y == F.y)
			{
				iReSpawn = 1;
				break;
			}
		}
		if (iReSpawn == 0)
		{
			MoveCursor(F.y + posy, F.x + posx);
			TextColor(10);
			cout << "O";
			TextColor(15);
			Fruit.push_back(F);
			count++;
		}
	}
}
bool Snake::IsGameOver()
{
	for (int i = 0; i < PointMap.size(); i++)
	{
		if (Ran[0].x == PointMap[i].x && Ran[0].y == PointMap[i].y)
			return true;
	}

	for (int i = 1; i < Ran.size(); i++)
	{
		if (Ran[0].x == Ran[i].x && Ran[0].y == Ran[i].y)
			return true;
	}

	return false;
}
void Snake::Play()
{
	ChoseSpeed();
	int iCountFruit = 0;
	int k = 3;
	Draw();
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if ((a == 'a' || a == 75) && k != 4) k = 2;
			else if ((a == 's' || a == 80) && k != 1) k = 3;
			else if ((a == 'w' || a == 72) && k != 3) k = 1;
			else if ((a == 'd' || a == 77) && k != 2) k = 4;
			int other = int(a);
			if (other == 32)//Nut Space
			{
				Pause();
			}
			else if (other == 27)// Nut Escape
			{
				Exit();
				return;
			}
		}

		EatFruit(iCountFruit);

		if (IsGameOver())
		{
			GameOver();
			break;
		}

		Sleep(550 / speed);
		Move(k);
	}
}
void Snake::ChoseSpeed()
{
	system("cls");
	cout << "\n\n\n";
	int k = 1;
	while (1)
	{
		MoveCursor(0, 0);
		if (_kbhit())
		{
			char a = _getch();
			if (a == 72)
			{
				k--;
				if (k == 0) k = 5;
			}
			if (a == 80)
			{
				k++;
				if (k == 6) k = 1;
			}
			if (a == 32 || a == 13)
			{
				speed = k * 2;
				break;
			}
		}
		TextColor(9);
		cout << "\n\n\n\n\t\t\t==== Chose Speed Mode ====" << "\n";
		if (k == 1)
		{
			TextColor(11);
			cout << "\t\t\t  >  Slow x1" << "\n";
			TextColor(9);
		}
		else cout << "\t\t\t     Slow x1" << "\n";

		if (k == 2)
		{
			TextColor(11);
			cout << "\t\t\t  >  Normal x4" << "\n";
			TextColor(9);
		}
		else cout << "\t\t\t     Normal x4" << "\n";
		if (k == 3)
		{
			TextColor(11);
			cout << "\t\t\t  >  Fast x6" << "\n";
			TextColor(9);
		}
		else cout << "\t\t\t     Fast x6" << "\n";
		if (k == 4)
		{
			TextColor(11);
			cout << "\t\t\t  >  Very Fast x8" << "\n";
			TextColor(9);
		}
		else cout << "\t\t\t     Very Fast x8" << "\n";
		if (k == 5)
		{
			TextColor(11);
			cout << "\t\t\t  >  Epic x10" << "\n";
			TextColor(9);
		}
		else cout << "\t\t\t     Epic x10" << "\n";
		TextColor(15);
	}

}
void Snake::Pause()
{
	MoveCursor(45, 8);
	TextColor(3);
	cout << "PAUSING";
	TextColor(15);
	_getch();
}
void Snake::GetMap(int file)
{
	ifstream f;
	switch (file)
	{
	case 1:
		f.open("map1.txt", ios::in);
		break;
	case 2:
		f.open("map2.txt", ios::in);
		break;
	default:
		break;
	}

	char Temp[100];
	while (!f.eof())
	{
		f.getline(Temp, 100);
		game.push_back(Temp);
	}
	f.close();
	for (int i = 0; i < game.size(); i++)
	{
		int length = game[i].length();
		for (int j = 0; j < length; j++)
		{
			if (game[i][j] == '1')
			{
				Point temp;
				temp.x = i;
				temp.y = j;
				PointMap.push_back(temp);
			}
		}
	}
}