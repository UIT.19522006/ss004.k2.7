#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
#include <conio.h>
#include <Windows.h>
#include <windows.h>

#define BLACK_COLOR			0
#define DARK_BLUE_COLOR		1
#define DARK_GREEN_COLOR	2
#define DARK_CYAN_COLOR		3
#define DARK_RED_COLOR		4
#define DARK_PINK_COLOR		5
#define DARK_YELLOW_COLOR	6
#define DARK_WHITE_COLOR	7
#define GREY_COLOR			8
#define BLUE_COLOR			9
#define GREEN_COLOR			10
#define CYAN_COLOR			11
#define RED_COLOR			12
#define PINK_COLOR			13
#define YELLOW_COLOR		14
#define WHITE_COLOR			15
// Game board position
#define posx 5
#define posy 2
#define maxFruit 3

using namespace std;
class Snake
{
private:
	struct Point
	{
		int x, y;
	};
	int mode;
	vector<string> game;
	//Man hinh 73x36
	vector<string> Display;
	vector<Point> Ran;
	vector <Point> PointMap;
	vector <string>Map;
	vector<Point> Fruit;
	int score;
	int speed;
public:
	void InitDisplay();
	void OutputDisplay();
	void Draw();
	Snake();
	Snake(int);
	void Spawn();
	void Move(int);
	void SpawnFruit(int&);
	void EatFruit(int&);
	bool IsGameOver();
	void Play();
	void ChoseSpeed();
	void GameOver();
	void Pause();
	void GetMap(int);
};
void Nocursortype();
void SetFontSize(const int& x, const int& y);
void MoveCursor(const int& x, const int& y);
void SetWindowSize(const int& x, const int& y);
void CenterWindow();
void FixWindowSize();
void StandardScreen();
void TextColor(int color);
void OutputIntro();
void Exit();
void HighScore();
void Tutor();
void OutputMenu(const int&);
void RunMenu();
