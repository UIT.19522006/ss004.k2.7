#pragma once
#include <Windows.h>
class Screen
{
public:
	void Nocursortype();
	void SetFontSize(const int& x, const int& y);
	void SetWindowSize(const int& x, const int& y);
	void CenterWindow();
	void FixWindowSize();
	void StandardScreen();
};

