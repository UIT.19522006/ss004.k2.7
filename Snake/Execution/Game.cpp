#include "Game.h"
void Game::OutputIntro()
{
	TextColor(12);
	cout << "\n\n\n\n\n";
	cout << "    ||========  ||\\\\     ||        //\\\\        ||    //  ||=========" << endl;
	cout << "    ||          || \\\\    ||       //  \\\\       ||  //    ||" << endl;
	cout << "    ||          ||  \\\\   ||      //    \\\\      ||//      ||" << endl;
	cout << "    ||========  ||   \\\\  ||     //======\\\\     ||        ||=========" << endl;
	cout << "            ||  ||    \\\\ ||    //        \\\\    ||\\\\      ||" << endl;
	cout << "            ||  ||     \\\\||   //          \\\\   ||  \\\\    ||" << endl;
	cout << "    ========||  ||      \\\\|  //            \\\\  ||    \\\\  ||=========" << endl << "\n";
	TextColor(1);
	cout << "                                                   SS004.K2.7" << "\n";
	TextColor(2);
	cout << "                                            19520022: Pham Ngoc Cam     " << "\n";
	cout << "                                            19520072: Le Kim Hang       " << "\n";
	cout << "                                            19520365: Do Thi Thanh An   " << "\n";
	cout << "                                            19521922: Tran Luong Nguyen " << "\n";
	cout << "                                            19522006: Nguyen Cong Phi   " << "\n";
	TextColor(15);
	int c = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 13)
			{
				break;
			}
		}
		Sleep(400);
		MoveCursor(19, 19);
		if (c == 0)
		{
			cout << "     Press Enter to continue     ";
			c++;
		}
		else
		{
			cout << "                                   ";
			c--;
		}
	}
	system("cls");
	MoveCursor(0, 0);
}
void Game::OutputMenu(const int& k)
{
	MoveCursor(22, 5);
	TextColor(15);
	cout << "========== MENU ==========";
	MoveCursor(28, 6);
	if (k == 1)
	{
		TextColor(10);
		cout << ">  Play";
		TextColor(15);
	}
	else cout << "   Play";
	MoveCursor(28, 7);
	if (k == 2)
	{
		TextColor(10);
		cout << ">  High score";
		TextColor(15);
	}
	else cout << "   High score";
	MoveCursor(28, 8);
	if (k == 3)
	{
		TextColor(10);
		cout << ">  Quick tutor";
		TextColor(15);
	}
	else cout << "   Quick tutor";
	MoveCursor(28, 9);
	if (k == 4)
	{
		TextColor(10);
		cout << ">  Add new map";
		TextColor(15);
	}
	else cout << "   Add new map";
	MoveCursor(28, 10);
	if (k == 5)
	{
		TextColor(10);
		cout << ">  Exit";
		TextColor(15);
	}
	else cout << "   Exit";
	MoveCursor(22, 11);
	cout << "=========== END ===========";
}
void Game::Exit()
{
	system("cls");
	int k = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 80 || a == 72 || a == 32)
			{
				if (k == 1) k = 0;
				else k = 1;
			}
			if (a == 13)
			{
				if (k == 0)
				{
					TextColor(15);
					system("cls");
					system("PAUSE");
					exit(0);
				}
				else return;
			}
		}
		MoveCursor(20, 5);
		cout << " ========== QUERY ==========";
		MoveCursor(20, 6);
		cout << "   Are you sure to quit ??? ";
		MoveCursor(29, 7);
		if (k == 0)
		{
			TextColor(14);
			cout << ">  Yes";
			TextColor(15);
		}
		else cout << "   Yes";
		MoveCursor(27, 8);
		if (k == 1)
		{
			TextColor(14);
			cout << "  >  No";
			TextColor(15);
		}
		else cout << "     No";
		MoveCursor(21, 9);
		cout << "=========== END ===========";
		MoveCursor(0, 0);
	}
	TextColor(15);
}
void Game::HighScore()
{
	system("cls");
	ifstream f;
	f.open("HighScore.txt", ios::in);
	if (!f.is_open())
	{
		cout << "Can't open HighScore.txt";
		_getch();
		return;
	}
	vector<string> name;
	vector<int> score;
	while (!f.eof())
	{
		string a;
		string b;
		int diem;

		getline(f, a);
		if (a.size() == 0) break;

		int pos = a.find(':');
		for (int i = pos + 1; i < a.size(); i++)
		{
			b += a[i];
		}
		a.erase(a.begin() + pos, a.end());
		stringstream ss(b);
		ss >> diem;

		name.push_back(a);
		score.push_back(diem);
	}
	MoveCursor(20, 5);
	cout << "==========High scores==========\n";
	if (name.size() == 0)
	{
		cout << "\n\n";
		cout << "                               No data";
		cout << "\n\n";
	}
	else
		for (int i = 0; i < name.size(); i++)
		{
			cout<<"\t\t\t" << i + 1 << ". " << name[i] << " : " << score[i] << "\n\n";
		}
	cout << "                        ==========END=========";
	f.close();
	_getch();
}
void Game::Tutor()
{
	system("cls");
	MoveCursor(16, 5);
	cout << "\u001b[37;1m======== QUICK TUTOR ========\n\n";
	cout << "                 \u001b[36mW or Arrow up\u001b[37;1m : up\n";
	cout << "                 \u001b[36mD or Arrow down\u001b[37;1m : down\n";
	cout << "                 \u001b[36mA or Arrow left\u001b[37;1m : left\n";
	cout << "                 \u001b[36mS or Arrow right\u001b[37;1m : right\n";
	cout << "                 \u001b[36mSpace Bar \u001b[37;1m: pause and move selection\n";
	cout << "                 \u001b[36mEsc \u001b[37;1m: exit\n";
	cout << "                 \u001b[36mEnter \u001b[37;1m: chose\n\n";
	cout << "                ============ END ============";
	_getch();
}
void Game::RunMenu()
{
	OutputMenu(1);
	int arrow = 1;
	while (1)
	{
		if (_kbhit())
		{
			char c = _getch();
			if (c == 13)
			{
				if (arrow == 1)
				{
					//// Start Game
					ClassicMode mode;
					mode.InitDisplay();
					mode.Play();
					system("cls");
					RunMenu();
				}
				if (arrow == 2)
				{
					HighScore();
					system("cls");
					OutputMenu(arrow);
				}
				if (arrow == 3)
				{
					Tutor();
					system("cls");
					OutputMenu(arrow);
				}
				if (arrow == 4)
				{
					AddMap();
					system("cls");
					OutputMenu(arrow);
				}
				if (arrow == 5)
				{
					Exit();
					system("cls");
					OutputMenu(arrow);
				}
			}
			if (c == 32)
			{
				if (arrow == 4) arrow = 1;
				else arrow++;
				MoveCursor(0, 0);
				OutputMenu(arrow);
			}
			int a = (int)c;
			if (a == 80)
			{
				if (arrow == 5) arrow = 1;
				else arrow++;
				OutputMenu(arrow);
			}
			if (a == 72)
			{
				if (arrow == 1) arrow = 5;
				else arrow--;
				OutputMenu(arrow);
			}
		}
		MoveCursor(0, 0);
	}
}
void Game::Run()
{
	OutputIntro();
	RunMenu();
}
void Game::AddMap()
{
	system("cls");
	MoveCursor(15, 5);
	cout << "=====What do you want to do ?=====";
	int choice = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 72 || a == 80)
			{
				choice++;
				choice %= 2;
			}
			else if (a == 13)
			{
				break;
			}
		}
		MoveCursor(25, 7);
		if (choice == 0)
		{
			TextColor(9);
			cout << "> Load File txt";
			TextColor(15);
		}
		else cout << "  Load File txt";

		MoveCursor(25, 9);
		if (choice == 1)
		{
			TextColor(9);
			cout << "> Custom Mode";
			TextColor(15);
		}
		else cout << "  Custom Mode";
	}

	if(choice==0)
	{
		system("cls");
		ifstream newMap;
		fstream listMap;
		char fileName[200];
		TextColor(10);
		MoveCursor(21, 5);
		cout << "Hay nhap duong dan chua Map:";
		MoveCursor(21, 7);
		cout << "EX: C:\\Users\ASUS\Desktop\map.txt \n";
		cin.getline(fileName, 200);
		newMap.open(fileName, ios::in);
		if (newMap.fail())
		{
			TextColor(13);
			MoveCursor(33,9);
			cout << "ERROR" << endl;
			system("pause");
			TextColor(15);
		}
		else
		{
			listMap.open("listMap.txt", ios::out | ios::app);
			listMap << fileName << endl;
			listMap.close();
			TextColor(14);
			MoveCursor(27,9);
			cout << "Add map successed" << endl;
			TextColor(15);
			Sleep(1000);
			_getch();
		}
	}
	else
	{
		CustomMap();
	}
}
void Game::CustomMap()
{
	system("cls");
	vector<string> mp(30,"                              ");
	MoveCursor(19, 4);
	for (int i = 0; i < 32; i++)
	{
		cout << "+";
	}
	MoveCursor(19,35);
	for (int i = 0; i < 32; i++)
	{
		cout << "+";
	}
	for (int i = 0; i < 30; i++)
	{
		MoveCursor(19, 5 + i);
		cout << "+                              +";
	}
	char gray = 176;
	for (int i = 1; i <= 5; i++)
	{
		for (int j = 1; j <= 5; j++)
		{
			MoveCursor(i + 20, j + 5);
			cout << gray;
		}
	}
	MoveCursor(0, 1);
	TextColor(7);
	cout << "Type any character in your keyboard to insert obstacles to you map.\n";
	cout << gray << " is where the Snake spawn.\nType any character in there will cause error.";
	TextColor(15);

	MoveCursor(2, 8);
	cout << "\u001b[31mESC :\u001b[37m";
	MoveCursor(2, 9);
	cout << "Quit edit map";

	MoveCursor(2, 12);
	cout << "\u001b[31mBackspace :\u001b[37m";
	MoveCursor(2, 13);
	cout << "Delete Character";

	MoveCursor(55, 8);
	cout << "\u001b[31mUp\u001b[37m";
	MoveCursor(55, 9);
	cout << "Move Cursor Up";

	MoveCursor(55, 12);
	cout << "\u001b[31mDown\u001b[37m";
	MoveCursor(55, 13);
	cout << "Move Cursor Down";

	MoveCursor(55, 16);
	cout << "\u001b[31mLeft\u001b[37m";
	MoveCursor(55, 17);
	cout << "Move Cursor Left";

	MoveCursor(55, 20);
	cout << "\u001b[31mRight\u001b[37m";
	MoveCursor(55, 21);
	cout << "Move Cursor Right";


	int k = 0;
	MoveCursor(20, 5);
	int x = 20, y = 5;
	double timer = clock();
	double t = clock() - timer;
	while (1)
	{
		t += clock() - timer;
		timer = clock();
		MoveCursor(x,y);
		if (_kbhit())
		{
			unsigned char a = _getch();
			if (a == 224)
			{
				a = _getch();

				if (a == 72)
				{
					if ((x-20 >= 1 && x-20 <= 5) && (y-5 >= 1 && y-5 <= 5))
					{
						cout << gray;
					}
					else
						cout << mp[y - 5][x - 20];
					y--;
					if (y == 4) y = 34;
					MoveCursor(x, y);			
				}
				else if (a == 80)
				{
					if ((x - 20 >= 1 && x - 20 <= 5) && (y - 5 >= 1 && y - 5 <= 5))
					{
						cout << gray;
					}
					else 
						cout << mp[y - 5][x - 20];
					y++;
					if (y == 35) y = 5;
					MoveCursor(x, y);	
				}
				else if (a == 75)
				{
					if ((x - 20 >= 1 && x - 20 <= 5) && (y - 5 >= 1 && y - 5 <= 5))
					{
						cout << gray;
					}
					else
						cout << mp[y - 5][x - 20];
					x--;
					if (x == 19) x = 49;
					MoveCursor(x, y);
				}
				else if (a == 77)
				{
					if ((x - 20 >= 1 && x - 20 <= 5) && (y - 5 >= 1 && y - 5 <= 5))
					{
						cout << gray;
					}
					else
						cout << mp[y - 5][x - 20];
					x++;
					if (x == 50) x = 20;
					MoveCursor(x, y);
				}
			}
			else if (a == 27)
			{
				system("cls");
				MoveCursor(20, 5);
				cout << "Do you want to save this map ?";
				int choice = 0;
				while (1)
				{
					if (_kbhit())
					{
						char temp = _getch();
						if (temp == 13)
						{
							if (choice == 0) break;
							else return;
						}
						if (temp == 72 || temp == 80)
						{
							choice++;
							choice %= 2;
						}
					}
					//Menu save
					MoveCursor(28, 7);
					if (choice == 0)
					{
						TextColor(9);
						cout << ">   Yes";
						TextColor(15);
					}
					else cout << "    Yes";
					MoveCursor(28, 8);
					if (choice == 1)
					{
						TextColor(9);
						cout << ">   No";
						TextColor(15);
					}
					else cout << "    No";
				}
				string mpname = "map1.txt";
				ifstream test(mpname);
				while (test.is_open())
				{
					mpname[3]++;
					test.close();
					test.open(mpname);
				}
				ofstream map(mpname);
				for (int i = 1; i <= 5; i++)
				{
					for (int j = 1; j <= 5; j++)
					{
						mp[i][j] = ' ';
					}
				}
				for (int i = 0; i < 30; i++)
				{
					map << mp[i] << "\n";
				}
				map.close();
				fstream listMap;
				listMap.open("listMap.txt", ios::out | ios::app);
				listMap << mpname << endl;
				listMap.close();
				system("cls");
				MoveCursor(20,5);
				cout << "Your map successfully saved as " << mpname;
				Sleep(1000);
				_getch();
				return;
			}
			else if (a == 8)
			{
				if ((x - 20 >= 2 && x - 20 <= 6) && (y - 5 >= 1 && y - 5 <= 5));
				else
				{
					if (x - 20 == 1 && (y - 5 >= 1 && y - 5 <= 5)) cout << gray;
					else cout << mp[y - 5][x - 20];
					x--;
					if (x == 19) x = 49;
					mp[y - 5][x - 20] = ' ';
					MoveCursor(x, y);
				}
			}
			else if ((a >= 33 && a <= 126)||a==32)
			{
				if ((x - 20 >= 1 && x - 20 <= 5) && (y - 5 >= 1 && y - 5 <= 5));
				else
				{
					mp[y - 5][x - 20] = a;
					cout << mp[y - 5][x - 20];
					x++;
					if (x == 50)
					{
						x = 20;
						y++;
						if (y == 35) y = 5;
					}
					MoveCursor(x, y);
				}	
			}
		}
		if (t > 500)
		{
			t = 0.0;
			k++;
			k = k % 2;
		}
		if (k ==1)
		{
			cout << "_";
		}
		else 
		{
			cout << mp[y-5][x-20];
		}
	}
}
