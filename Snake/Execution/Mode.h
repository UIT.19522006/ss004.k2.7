#pragma once
#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
#include <conio.h>
#include <Windows.h>
#include <windows.h>

//Color declaration
#define BLACK_COLOR			0
#define DARK_BLUE_COLOR		1
#define DARK_GREEN_COLOR	2
#define DARK_CYAN_COLOR		3
#define DARK_RED_COLOR		4
#define DARK_PINK_COLOR		5
#define DARK_YELLOW_COLOR	6
#define DARK_WHITE_COLOR	7
#define GREY_COLOR			8
#define BLUE_COLOR			9
#define GREEN_COLOR			10
#define CYAN_COLOR			11
#define RED_COLOR			12
#define PINK_COLOR			13
#define YELLOW_COLOR		14
#define WHITE_COLOR			15
//board position
#define posx 5
#define posy 2

#define maxFruit 3

using namespace std;
class Mode
{
protected:
	struct Point
	{
		int x, y;
	};
	int mode;
	//Man hinh 73x36
	vector<string> Display;
	vector<string> game;
	vector<Point> Ran;
	int score;
public:
	virtual void InitDisplay();
	virtual void OutputDisplay();
	void GameOver();
	void Pause();
	void Spawn();
	virtual void Move(const int&) =0;
	virtual bool IsGameOver() const=0;
	virtual void Play()=0;
};
void TextColor(const int&);
void MoveCursor(const int& , const int& );


