#include "Screen.h"
void Screen::SetFontSize(const int& x, const int& y)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_FONT_INFOEX lpConsoleCurrentFontEx;
	lpConsoleCurrentFontEx.cbSize = sizeof(lpConsoleCurrentFontEx);
	lpConsoleCurrentFontEx.dwFontSize.X = x;
	lpConsoleCurrentFontEx.dwFontSize.Y = y;
	lpConsoleCurrentFontEx.FontFamily = FF_DONTCARE;
	lpConsoleCurrentFontEx.FontWeight = FW_BOLD;
	SetCurrentConsoleFontEx(out, false, &lpConsoleCurrentFontEx);
}
void Screen::SetWindowSize(const int& x, const int& y)
{
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, x, y, TRUE);
}
void Screen::CenterWindow()
{
	HWND console = GetConsoleWindow();
	RECT rectClient, rectWindow;
	GetClientRect(console, &rectClient);
	GetWindowRect(console, &rectWindow);
	int px, py;
	px = GetSystemMetrics(SM_CXSCREEN) / 2 - (rectWindow.right - rectWindow.left) / 2;
	py = GetSystemMetrics(SM_CYSCREEN) / 2 - (rectWindow.bottom - rectWindow.top) / 2;
	MoveWindow(console, px, py, rectClient.right - rectClient.left, rectClient.bottom - rectClient.top, TRUE);
}
void Screen::FixWindowSize()
{
	HWND consoleWindow = GetConsoleWindow();
	LONG style = GetWindowLong(consoleWindow, GWL_STYLE);
	style = style & ~(WS_MAXIMIZEBOX) & ~(WS_THICKFRAME);
	SetWindowLong(consoleWindow, GWL_STYLE, style);
}
void Screen::Nocursortype() // An con tro tren man hinh console;
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO Info;
	Info.bVisible = FALSE;
	Info.dwSize = 20;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
}
void Screen::StandardScreen()
{
	SetFontSize(18, 18);
	SetWindowSize(1920, 1080);
	CenterWindow();
	FixWindowSize();
	Nocursortype();
}
