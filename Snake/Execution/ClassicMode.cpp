#include "ClassicMode.h"
ClassicMode::ClassicMode()
{
	int iMap = 0;
	fstream listMap;
	listMap.open("listMap.txt", ios::in);
	vector <string> ListName;
	char Name[200];
	while (!listMap.eof())
	{
		listMap.getline(Name, 200);
		if (Name[0] != '\0')
		{
			ListName.push_back(Name);
			iMap++;
		}
	}
	listMap.close();
	system("cls");
	int k = 1;
	while (1)
	{
		MoveCursor(20, 5);
		cout << "Which map do you want to chose?"<<"\n\n";
		if (_kbhit())
		{
			char a = _getch();
			if (a == 13)
			{
				break;
			}
			if (a == 72)
			{
				k--;
				if (k == 0) k = iMap;
			}
			if (a == 80)
			{
				k++;
				if (k == iMap + 1) k = 1;
			}
		}
		for (int i = 0; i < ListName.size(); i++)
		{
			if (k == i + 1)
			{
				TextColor(13);
				cout << "\t\t\t > " << ListName[i] << "\n\n";
				TextColor(15);
			}
			else
				cout << "\t\t\t > " << ListName[i] << "\n\n";
		}
	}
	GetMap(k - 1);
	Spawn();
}
void ClassicMode::SpawnFruit(int& count)
{
	while (count < maxFruit)
	{
		int iReSpawn = 0;
		Point F;
		F.x = rand() % 25 + 2;
		F.y = rand() % 25 + 2;
		for (int i = 0; i < PointMap.size(); i++)
		{
			if (PointMap[i].x == F.x && PointMap[i].y == F.y)
			{
				iReSpawn = 1;
				break;
			}

		}
		for (int i = 0; i < Ran.size(); i++)
		{
			if (Ran[i].x == F.x && Ran[i].y == F.y)
			{
				iReSpawn = 1;
				break;
			}
		}
		if (iReSpawn == 0)
		{
			MoveCursor(F.y + posy, F.x + posx);
			TextColor(10);
			cout << "O";
			TextColor(15);
			Fruit.push_back(F);
			count++;
		}
	}
}
void ClassicMode::EatFruit(int& count)
{
	for (int i = 0; i < Fruit.size(); i++)
	{
		if (Ran[0].x == Fruit[i].x && Ran[0].y == Fruit[i].y)
		{
			count--;
			Point a = Ran[Ran.size() - 1];
			Point b = Ran[Ran.size() - 2];
			Point Tail;
			if (a.x == b.x)
			{
				if (a.y == 0)
				{
					Tail.x = a.x;
					Tail.y = 1;
				}
				else if (a.y == 29)
				{
					Tail.x = a.x;
					Tail.y = 28;
				}
				else if (a.y > b.y)
				{

					Tail.x = a.x;
					Tail.y = a.y + 1;
				}
				else
				{
					Tail.x = a.x;
					Tail.y = a.y - 1;
				}

				Ran.push_back(Tail);

			}
			else
			{
				if (a.x == 0)
				{
					Tail.x = 1;
					Tail.y = a.y;
				}
				else if (a.x == 29)
				{
					Tail.x = 28;
					Tail.y = a.y;
				}
				else if (a.x > b.x)
				{
					Tail.x = a.x + 1;
					Tail.y = a.y;
				}
				else
				{
					Tail.x = a.x - 1;
					Tail.y = a.y;
				}
				Ran.push_back(Tail);
			}
			Fruit.erase(Fruit.begin() + i);
			//Diem
			score += speed;
			string h = to_string(score);
			while (h.size() < 8)
			{
				h = '0' + h;
			}
			TextColor(11);
			MoveCursor(46, 28);
			cout << h;
			TextColor(15);
		}
	}
	SpawnFruit(count);
}
void ClassicMode::Move(const int& way)
{
	MoveCursor(0, 0);
	MoveCursor(Ran[Ran.size() - 1].y + posy, Ran[Ran.size() - 1].x + posx);
	cout << " ";
	Point a = Ran[0];
	for (int i = 0; i < Ran.size() - 1; i++)
	{
		string k = "\u001b[38;5;";
		MoveCursor(Ran[i].y + posy, Ran[i].x + posx);
		if (i % 2 == 0)
		{
			k += to_string(skin.x);
			k += "mX";
			cout << k;
		}
		else
		{
			k += to_string(skin.y);
			k += "mX";
			cout << k;
		}
	}
	if (way == 1)
	{
		a.x--;
		if (a.x < 0)
		{
			a.x = 29;
		}
		MoveCursor(a.y + posy, a.x + posx);
	}
	if (way == 2)
	{
		a.y--;
		if (a.y < 0)
		{
			a.y = 29;
		}
		MoveCursor(a.y + posy, a.x + posx);
	}
	if (way == 3)
	{
		a.x++;
		if (a.x > 29)
		{
			a.x = 0;
		}
		MoveCursor(a.y + posy, a.x + posx);
	}
	if (way == 4)
	{
		a.y++;
		if (a.y > 29)
		{
			a.y = 0;
		}
		MoveCursor(a.y + posy, a.x + posx);
	}
	cout << "\u001b[38;5;201mR\u001b[0m";
	Ran.insert(Ran.begin(), a);
	Ran.erase(Ran.begin() + Ran.size() - 1);
	MoveCursor(0, 0);
}
bool ClassicMode::IsGameOver() const
{
	for (int i = 0; i < PointMap.size(); i++)
	{
		if (Ran[0].x == PointMap[i].x && Ran[0].y == PointMap[i].y)
			return true;
	}

	for (int i = 1; i < Ran.size(); i++)
	{
		if (Ran[0].x == Ran[i].x && Ran[0].y == Ran[i].y)
			return true;
	}

	return false;
}
void ClassicMode::Play()
{
	ChoseSpeed();
	GetSkin();
	int iCountFruit = 0;
	int k = 3;
	Draw();
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if ((a == 'a' || a == 75) && k != 4) k = 2;
			else if ((a == 's' || a == 80) && k != 1) k = 3;
			else if ((a == 'w' || a == 72) && k != 3) k = 1;
			else if ((a == 'd' || a == 77) && k != 2) k = 4;
			int other = int(a);
			if (other == 32)//Nut Space
			{
				Pause();
			}
		}

		EatFruit(iCountFruit);

		if (IsGameOver())
		{
			GameOver();
			break;
		}

		Sleep(550 / speed);
		Move(k);
	}
}
void ClassicMode::ChoseSpeed()
{
	system("cls");
	cout << "\n\n\n";
	int k = 1;
	while (1)
	{
		MoveCursor(20,5);
		if (_kbhit())
		{
			char a = _getch();
			if (a == 72)
			{
				k--;
				if (k == 0) k = 5;
			}
			if (a == 80)
			{
				k++;
				if (k == 6) k = 1;
			}
			if (a == 32 || a == 13)
			{
				speed = k * 2;
				break;
			}
		}
		cout << "==== Chose Speed Mode ====" << "\n\n";
		if (k == 1)
		{
			TextColor(11);
			cout << "\t\t\t  >  Slow x2" << "\n\n";
			TextColor(15);
		}
		else cout << "\t\t\t     Slow x2" << "\n\n";

		if (k == 2)
		{
			TextColor(11);
			cout << "\t\t\t  >  Normal x4" << "\n\n";
			TextColor(15);
		}
		else cout << "\t\t\t     Normal x4" << "\n\n";
		if (k == 3)
		{
			TextColor(11);
			cout << "\t\t\t  >  Fast x6" << "\n\n";
			TextColor(15);
		}
		else cout << "\t\t\t     Fast x6" << "\n\n";
		if (k == 4)
		{
			TextColor(11);
			cout << "\t\t\t  >  Very Fast x8" << "\n\n";
			TextColor(15);
		}
		else cout << "\t\t\t     Very Fast x8" << "\n\n";
		if (k == 5)
		{
			TextColor(11);
			cout << "\t\t\t  >  Epic x10" << "\n\n";
			TextColor(15);
		}
		else cout << "\t\t\t     Epic x10" << "\n\n";
		TextColor(15);
	}
	system("cls");
}
void ClassicMode::Draw()
{
	TextColor(15);
	string draw;
	for (int i = 0; i < game.size(); i++)
	{
		for (int j = 0; j < game[i].length(); j++)
		{
			Display[i + 5][j + 2] = game[i][j];
		}
	}
	for (int i = 0; i < Display.size(); i++)
	{
		draw += Display[i] + "\n";
	}
	cout << draw;
}
void ClassicMode::GetMap(const int& file)
{
	ifstream listMap;
	listMap.open("listMap.txt", ios::in);
	if (listMap.fail())
	{
		cout << "ERROR" << endl;
	}
	else
	{
		char fileName[200];
		int icount = 0;
		while (!listMap.eof())
		{
			listMap.getline(fileName, 200);
			if (icount == file)
			{
				break;
			}
			else
				icount++;
		}
		listMap.close();

		ifstream map;
		map.open(fileName, ios::in);
		char Temp[100];
		if (map.fail())
		{
			cout << "ERROR" << endl;
		}
		else
		{
			while (!map.eof())
			{
				map.getline(Temp, 100);
				game.push_back(Temp);
			}
			map.close();
			for (int i = 0; i < game.size(); i++)
			{
				int length = game[i].length();
				for (int j = 0; j < length; j++)
				{
					if (game[i][j] != ' ')
					{
						Point temp;
						temp.x = i;
						temp.y = j;
						PointMap.push_back(temp);
					}
				}
			}
		}
	}
}
void ClassicMode::GetSkin()
{
	system("cls");
	MoveCursor(20, 5);
	cout << "Which skin do you want to choose?";
	int k = 0;
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 80)
			{
				k++;
				k %= 3;
			}
			if (a == 72)
			{
				k--;
				if (k == -1)
				k = 2;
			}
			if (a == 13)
				break;
		}
		MoveCursor(30, 7);
		if (k == 0)
		{
			cout << ">   \u001b[38;5;184mX\u001b[38;5;202mX\u001b[38;5;184mX\u001b[38;5;202mX\u001b[37;1m";
		}
		else cout << "    \u001b[38;5;184mX\u001b[38;5;202mX\u001b[38;5;184mX\u001b[38;5;202mX\u001b[37;1m";
		MoveCursor(30, 8);
		if (k == 1)
		{
			cout << ">   \u001b[38;5;40mX\u001b[38;5;220mX\u001b[38;5;40mX\u001b[38;5;220mX\u001b[37;1m";
		}
		else cout << "    \u001b[38;5;40mX\u001b[38;5;220mX\u001b[38;5;40mX\u001b[38;5;220mX\u001b[37;1m";
		MoveCursor(30, 9);
		if (k == 2)
		{
			cout << ">   \u001b[38;5;17mX\u001b[38;5;208mX\u001b[38;5;17mX\u001b[38;5;208mX\u001b[37;1m";
		}
		else cout << "    \u001b[38;5;17mX\u001b[38;5;208mX\u001b[38;5;17mX\u001b[38;5;208mX\u001b[37;1m";
	}
	if (k == 0)
	{
		skin.x = 184;
		skin.y = 202;
	}
	else if (k == 1)
	{
		skin.x = 40;
		skin.y = 220;
	}
	else if (k == 2)
	{
		skin.x = 17;
		skin.y = 208;
	}
	system("cls");
}
