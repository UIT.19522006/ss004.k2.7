#include "Mode.h"
void TextColor(const int& color)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}
void MoveCursor(const int& x, const int& y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsole, c);
}
void Mode::InitDisplay()
{
	vector<string> title;
	string a = "------------------------------";
	string b = "---          \u001b[31;1mSNAKE\u001b[37;1m         ---";
	title.push_back(a);
	title.push_back(b);
	title.push_back(a);

	string c, d;
	vector<string> Gamestate;
	a = "----------------------------";
	b = "         \u001b[32mGAME STATE\u001b[37;1m";
	c = "    --------------------    ";
	d = "          \u001b[35mPLAYING\u001b[37m";

	Gamestate.push_back(a);
	Gamestate.push_back(b);
	Gamestate.push_back(c);
	Gamestate.push_back(d);
	Gamestate.push_back(a);

	vector<string> Map;
	Map.push_back(a);
	b = "         \u001b[33;1mCLASSIC MODE\u001b[37;1m  ";
	Map.push_back(b);
	Map.push_back(a);

	vector<string> Score;
	Score.push_back(a);
	b = "            \u001b[34;1mSCORE\u001b[37;1m       ";
	Score.push_back(b);
	Score.push_back(c);
	d = "           00000000         ";
	Score.push_back(d);

	string lim(73, ' ');

	for (int i = 0; i < 36; i++)
	{
		Display.push_back(lim);
	}


	MoveCursor(0, 0);
	for (int i = 0; i < title.size(); i++)
	{
		for (int j = 0; j < title[i].size(); j++)
		{
			Display[i + 1][j + 2] = title[i][j];
		}
	}
	for (int i = 0; i < Gamestate.size(); i++)
	{
		for (int j = 0; j < Gamestate[i].size(); j++)
		{
			Display[i + 5][j + 35] = Gamestate[i][j];
		}
	}
	for (int i = 0; i < Map.size(); i++)
	{
		for (int j = 0; j < Map[i].size(); j++)
		{
			Display[i + 15][j + 35] = Map[i][j];
		}
	}
	for (int i = 0; i < Score.size(); i++)
	{
		for (int j = 0; j < Score[i].size(); j++)
		{
			Display[i + 25][j + 35] = Score[i][j];
		}
	}
}
void Mode::OutputDisplay()
{
	string display;
	for (int i = 0; i < Display.size(); i++)
	{
		display += Display[i] + "\n";
	}
	TextColor(15);
	cout << display;
	MoveCursor(45, 8);
	TextColor(13);
	cout << "\u001b[32mPLAYING";
	TextColor(15);
}
void Mode::GameOver()
{
	MoveCursor(Ran[0].y + posy, Ran[0].x + posx);
	TextColor(12);
	cout << "R";
	TextColor(15);
	MoveCursor(45, 8);
	cout << "  \u001b[31;1mDEATH\u001b[37;1m  ";
	Sleep(2000);
	_getch();
	system("cls");
	MoveCursor(22, 5);
	cout << "+----------------------------+";
	MoveCursor(22, 6);
	cout << "|          Game Over         |";
	MoveCursor(22, 7);
	cout << "|   Better luck next time    |";
	MoveCursor(22, 8);
	cout << "+----------------------------+";
	int k = 1;
	_getch();
	system("cls");

	//Save game
	ifstream f;
	f.open("HighScore.txt", ios::in);
	if (!f.is_open())
	{
		MoveCursor(22, 16);
		cout << "Can't open HighScore.txt";
		_getch();
		return;
	}
	vector<string> name;
	vector<int> scores;
	while (!f.eof())
	{
		string a;
		string b;
		int diem;

		getline(f, a);
		if (a.size() == 0) break;

		int pos = a.find(':');
		for (int i = pos + 1; i < a.size(); i++)
		{
			b += a[i];
		}
		a.erase(a.begin() + pos, a.end());
		stringstream ss(b);
		ss >> diem;

		name.push_back(a);
		scores.push_back(diem);

	}
	f.close();

	if (scores.size() == 0 || scores.size() < 10 || score > scores[scores.size() - 1])
	{
		system("cls");
		MoveCursor(22, 5);
		cout << "Enter your name to save: ";
		string username;
		getline(cin, username);
		while (username.size() == 0)
		{
			MoveCursor(22, 6);
			cout << "Type your name again: ";
			getline(cin, username);
		}
		if (scores.size() == 0)
		{
			scores.push_back(score);
			name.push_back(username);
		}
		else
		{
			int i = 0;
			while (i < scores.size() && score < scores[i])
			{
				i++;
			}
			if (i >= scores.size())
			{
				scores.push_back(score);
				name.push_back(username);
			}
			else
			{
				scores.insert(scores.begin() + i, score);
				name.insert(name.begin() + i, username);
			}
		}
		cout << "a";
		if (scores.size() > 10)
		{
			scores.pop_back();
			name.pop_back();
		}
	}

	ofstream f1;
	f1.open("HighScore.txt", ios::trunc);
	if (!f1.is_open())
	{
		cout << "Can't open file to write";
	}
	for (int i = 0; i < scores.size(); i++)
	{
		f1 << name[i] << ':';
		f1 << scores[i];
		f1 << "\n";
	}
	f1.close();
}
void Mode::Pause()
{
	MoveCursor(45, 8);
	cout << "\u001b[36;1mPAUSING\u001b[37;1m";
	while (1)
	{
		if (_kbhit())
		{
			char a = _getch();
			if (a == 32)
			{
				break;
			}
		}
	}
	MoveCursor(45, 8);
	cout << "\u001b[35mPLAYING\u001b[37;1m";
	TextColor(15);
}
void Mode::Spawn()
{
	Point p;
	p.x = 3;
	p.y = 3;
	Ran.push_back(p);
	p.y--;
	Ran.push_back(p);
	p.y--;
	Ran.push_back(p);
}

