#pragma once
#include "Mode.h"
#include "ClassicMode.h"
class Game
{
public:
	void OutputIntro();
	void OutputMenu(const int&);
	void Exit();
	void HighScore();
	void Tutor();
	void Run();
	void RunMenu();
	void AddMap();
	void CustomMap();
};

