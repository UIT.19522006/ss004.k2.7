#pragma once
#include "Mode.h"
class ClassicMode:public Mode
{
protected:
	vector<Point> PointMap;
	vector<string> Map;
	vector<Point> Fruit;
	int speed;
	Point skin;
public:
	ClassicMode();
	void SpawnFruit(int&);
	void EatFruit(int&);
	void Move(const int&) ;
	bool IsGameOver() const;
	void Play();
	void ChoseSpeed();
	void Draw();
	void GetMap(const int&);
	void GetSkin();
};
